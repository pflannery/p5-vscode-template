# P5 js project template for vscode

Extensions required to run this simple project

- [Live Preview by Microsoft](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server)

Browsers

- Chrome or Mircosoft Edge

Run

- Start the live preview server (Ctrl+P then select "Live Preview: Start Server")
- Pressing F5 to run the sketch 
  - Alternatively goto the "Run and Debug" and press the play button