export function getRandomColour() {
  return [
    floor(random(255)),
    floor(random(255)),
    floor(random(255))
  ]
}

export function lerpColour(colour1, colour2, t) {
  return [
    floor(lerp(colour1[0], colour2[0], t)),
    floor(lerp(colour1[1], colour2[1], t)),
    floor(lerp(colour1[2], colour2[2], t))
  ]
}
