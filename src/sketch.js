/// <reference path="../node_modules/@types/p5/global.d.ts" />

import { getRandomColour, lerpColour } from "./utils.js";

let colour, nextColour
let changeInterval = 2000
let prevMs = 0;

function setup() {
  createCanvas(720, 400);

  // Init colors randomly
  colour = getRandomColour();
  nextColour = getRandomColour();

  // store the elapsed time
  prevMs = millis();
}

function draw() {
  background(32);
  strokeWeight(2);

  // get the elapsed time
  const ms = millis();

  // calculate the elapsed difference since last frame
  const delta = ms - prevMs;

  // lerp between current colour and the next colour
  colour = lerpColour(colour, nextColour, delta / changeInterval)

  // apply the lerped colour
  stroke(...colour);
  fill(...colour, 127);

  // Draw a circle in the centre
  circle(width / 2, height / 2, 200)

  // update the next colour
  if (delta > changeInterval) {
    nextColour = getRandomColour();
    prevMs = ms;
  }

}

// In js module mode we need to 
// map the setup and draw functions to the default p5 instance
window.setup = setup
window.draw = draw